import os
import time
from datetime import datetime

times = [100, 4500, 6000, 6490, 7000, 8000]

while True:
    now = datetime.now()
    nowSeconds = now.hour * 60 * 60 + now.minute * 60 + now.second
    print(nowSeconds)
    time.sleep(1)

    nextBusTimeId = 0
    for t in times:
        if nowSeconds - t < 0:
            break
        else:
            nextBusTimeId += 1

    print(nextBusTimeId, times[nextBusTimeId])
    print("Next Bus leaving in: ", times[nextBusTimeId] - nowSeconds)
