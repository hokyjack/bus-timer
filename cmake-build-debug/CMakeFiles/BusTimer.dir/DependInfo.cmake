# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/an/Projects/BusTimer/src/main.cpp" "/home/an/Projects/BusTimer/cmake-build-debug/CMakeFiles/BusTimer.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ARDUINO=10619"
  "ARDUINO_ARCH_AVR"
  "ARDUINO_AVR_NANO"
  "F_CPU=16000000L"
  "PLATFORMIO=30401"
  "__AVR_ATmega168__"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/an/.platformio/packages/framework-arduinoavr/cores/arduino"
  "/home/an/.platformio/packages/framework-arduinoavr/variants/eightanaloginputs"
  "../src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/LiquidCrystal/src"
  "../.piolibdeps/TM1637_ID258"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/__cores__/arduino/EEPROM/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/__cores__/arduino/HID/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/__cores__/arduino/SPI/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/__cores__/arduino/SoftwareSerial/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/__cores__/arduino/Wire/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/Adafruit_CircuitPlayground"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/Adafruit_CircuitPlayground/utility"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/Bridge/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/Esplora/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/Ethernet/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/Firmata"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/Firmata/utility"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/GSM/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/Keyboard/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/Mouse/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/RobotIRremote/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/Robot_Control/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/Robot_Motor/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/SD/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/Servo/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/SpacebrewYun/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/Stepper/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/TFT/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/Temboo/src"
  "/home/an/.platformio/packages/framework-arduinoavr/libraries/WiFi/src"
  "/home/an/.platformio/packages/toolchain-atmelavr/avr/include"
  "/home/an/.platformio/packages/toolchain-atmelavr/lib/gcc/avr/4.9.2/include"
  "/home/an/.platformio/packages/toolchain-atmelavr/lib/gcc/avr/4.9.2/include-fixed"
  "/home/an/.platformio/packages/tool-unity"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
