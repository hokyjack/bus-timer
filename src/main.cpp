#include "Arduino.h"
#include <LiquidCrystal.h>

#define FRI 5
#define SAT 6
#define SUN 0
#define DAYSECOND (60L * 60 * 24)
#define ENDFLAG 65000
#define DELAY 1000

#include "../include/workday_times_143.h"
#include "../include/saturday_times_143.h"
#include "../include/sunday_times_143.h"

#define GET_HOURS(x) (x / 3600)
#define GET_MINUTES(x) ((x % 3600) / 60)
#define GET_SECONDS(x) (x % 60)

#define CONTRAST 30
#define DAY_BRIGHTNESS 1000
#define EVENING_BRIGHTNESS 70
#define NIGHT_BRIGHTNESS 2


LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

// local vars
unsigned long next_bus_time, next_bus_time_diff;

unsigned long day_time = 0L*3600+27L*60; // 00:27 start time
int day_of_week = 5; // fri

#define COMPENSATION 1392L
unsigned long compensation_time = COMPENSATION;

unsigned int * next_time_ptr = NULL;

// getting garbled text after while when text set to MON
const char * weekday[] = { "SUN", "M0N", "TUE", "WED", "THU", "FRI", "SAT" };

bool day_brightness_set = false;
bool night_brightness_set = false;
bool evening_brightness_set = false;

static long lastUpdate;


void inc_day() {
    day_of_week = (day_of_week + 1) % 7;
}

void inc_day_time() {
    day_time = day_time + 1;

    if (day_time % DAYSECOND == 0) {
        day_time = 0;
        inc_day();
        next_time_ptr = NULL;
    }
}

unsigned long get_next_day_first_time() {
    switch (day_of_week) {
        case SAT:
            return *sunday_times_143;
        case SUN:
            return *workday_times_143;
        case FRI:
            return *saturday_times_143;
        default:
            return *workday_times_143;
    }
}

unsigned int * get_day_first_time_ptr() {
    switch(day_of_week) {
        case SAT:
            return saturday_times_143;
        case SUN:
            return sunday_times_143;
        default:
            return workday_times_143;
    }
}

void setup() {
    Serial.begin(9600); // begin serial
    analogWrite(6, CONTRAST); // set contrast o LCD
    analogWrite(9, DAY_BRIGHTNESS); // set LCD backlight
    lcd.begin(16, 2);
};

void loop() {

    // read current daytime as: d HH mm
    if (Serial.available()) {
        unsigned int hours = (unsigned int) Serial.parseInt();
        unsigned int minutes = (unsigned int) Serial.parseInt();
        day_of_week = (int) Serial.parseInt();
        day_time = hours * 3600 + minutes * 60;
    }

    if (millis() - lastUpdate > DELAY) {

        lastUpdate = millis();

        if (next_time_ptr == NULL) {
            next_time_ptr = get_day_first_time_ptr();
        }
        while (day_time > (*next_time_ptr) * 10L) {
            next_time_ptr++;
        }
        if (*next_time_ptr == ENDFLAG) {
            next_bus_time = get_next_day_first_time() * 10L;
            next_bus_time_diff = DAYSECOND - day_time + next_bus_time;
        } else {
            next_bus_time = (unsigned long) (*next_time_ptr) * 10L;
            next_bus_time_diff = next_bus_time - day_time;
        }

        // print outputs to LCD
        char first_row_buffer[9];
        sprintf(first_row_buffer, "%02lu:%02lu:%02lu",
                GET_HOURS(day_time), GET_MINUTES(day_time), GET_SECONDS(day_time));

        lcd.setCursor(0, 0);
        lcd.print(first_row_buffer);

        lcd.setCursor(13, 0);
        lcd.print(weekday[day_of_week]);

        char second_row_buffer[17];
        sprintf(second_row_buffer, "%02lu:%02lu:%02lu  %3lu:%02lu",
                GET_HOURS(next_bus_time), GET_MINUTES(next_bus_time), GET_SECONDS(next_bus_time),
                GET_MINUTES(next_bus_time_diff), GET_SECONDS(next_bus_time_diff));

        lcd.setCursor(0, 1);
        lcd.print(second_row_buffer);

        // check for DAY/NIGHT brightness

        // set EVENING
        if (GET_HOURS(day_time) > 19) {
            if (!evening_brightness_set) {
                analogWrite(9, EVENING_BRIGHTNESS); // set LCD backlight
                day_brightness_set = false;
                night_brightness_set = false;
                evening_brightness_set = true;
            }
        // set NIGHT
        } else if (GET_HOURS(day_time) < 6) {
            if (!night_brightness_set) {
                analogWrite(9, NIGHT_BRIGHTNESS); // set LCD backlight
                day_brightness_set = false;
                night_brightness_set = true;
                evening_brightness_set = false;
            }
        // st DAY
        } else {
            if (!day_brightness_set) {
                analogWrite(9, DAY_BRIGHTNESS); // set LCD backlight
                day_brightness_set = true;
                night_brightness_set = false;
                evening_brightness_set = false;
            }
        }

        //    Serial.println(first_row_buffer);
        //    Serial.println(second_row_buffer);

        // time compensation +1 sec each 1392 sec = 23 minutes
        compensation_time--;
        if (compensation_time == 0) {
            inc_day_time();
            compensation_time = COMPENSATION;
        }

        inc_day_time();
    }
}

